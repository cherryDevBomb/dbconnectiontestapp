package com.garmin.testapp.controller;

import com.garmin.testapp.model.User;
import com.garmin.testapp.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/testapp/users")
public class UserController {

    private final IUserService userService;

    @Autowired
    public UserController(IUserService userService) {
        this.userService = userService;
    }


    @RequestMapping(value = "/{username}", method = RequestMethod.GET)
    public ResponseEntity<?> getUser(@PathVariable String username) {

        User user = userService.findUser(username);
        if (user == null)
            return new ResponseEntity<String>("User not found", HttpStatus.NOT_FOUND);
        else
            return new ResponseEntity<User>(user, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.GET)
    public List<User> getAll() {
        return userService.findAll();
    }


    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> saveUser(@RequestBody User user) {
        User u = userService.saveUser(user);
        if (u != null) {
            return new ResponseEntity<String>("User saved", HttpStatus.OK);
        }
        else {
            return new ResponseEntity<String>("User already exists", HttpStatus.CONFLICT);
        }
    }

}
