package com.garmin.testapp.service;

import com.garmin.testapp.model.User;

import java.util.List;


public interface IUserService {

    User findUser(String username);
    List<User> findAll();
    User saveUser(User user);
}
