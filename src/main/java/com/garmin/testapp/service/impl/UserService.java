package com.garmin.testapp.service.impl;


import com.garmin.testapp.model.User;
import com.garmin.testapp.repository.UserRepository;
import com.garmin.testapp.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserService implements IUserService {

    private final UserRepository userRepo;

    @Autowired
    public UserService(UserRepository userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public User findUser(String username) {
        return userRepo.getUser(username);
    }

    @Override
    public List<User> findAll() {
        return userRepo.findAll();
    }

    @Override
    public User saveUser(User user) {
        return userRepo.addUser(user);
    }
}
