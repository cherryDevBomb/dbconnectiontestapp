package com.garmin.testapp.repository.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.TimeZone;


@Component
public class JdbcUtil {

    @Value("${jdbc.driverClassName}")
    private String driverClassName;

    @Value("${jdbc.url}")
    private String url;

    @Value("${jdbc.username}")
    private String username;

    @Value("${jdbc.password}")
    private String password;

    private static Connection connection = null;

    private Connection getNewConnection() {
        url = url + "?serverTimezone=" + TimeZone.getDefault().getID();
        Connection newConnection = null;
        try {
            newConnection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return newConnection;
    }


    public Connection getConnection() {
        try {
            if (connection == null || connection.isClosed())
                connection = getNewConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
