package com.garmin.testapp.repository;

import com.garmin.testapp.model.User;
import com.garmin.testapp.repository.utils.JdbcUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@Repository
public class UserRepository {

    private final JdbcUtil jdbcUtil;

    @Autowired
    public UserRepository(JdbcUtil jdbcUtil) {
        this.jdbcUtil = jdbcUtil;
    }


    public User getUser(String username) {
        Connection con = jdbcUtil.getConnection();

        try {
            PreparedStatement statement = con.prepareStatement("SELECT * from users WHERE username=?");
            statement.setString(1, username);
            try(ResultSet result = statement.executeQuery()) {
                if (result.next()) {
                    String user = result.getString("username");
                    String pass = result.getString("password");
                    return new User(user, pass);
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public List<User> findAll() {
        Connection con = jdbcUtil.getConnection();

        List<User> users = new ArrayList<>();
        try {
            PreparedStatement statement = con.prepareStatement("SELECT * from users");
            try(ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    String user = result.getString("username");
                    String pass = result.getString("password");
                    users.add(new User(user, pass));
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }


    public User addUser(User user) {
        Connection con = jdbcUtil.getConnection();
        try {
            PreparedStatement statement = con.prepareStatement("INSERT INTO users VALUES (?,?)");
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            int rowsAffected = statement.executeUpdate();
            if (rowsAffected > 0) {
                return user;
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }


}
